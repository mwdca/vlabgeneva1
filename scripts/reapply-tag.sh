#!/bin/bash
# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.
#

source .env
TAG=$CI_COMMIT_REF_NAME
echo TAG=$TAG
git tag -d $TAG || true
git push --delete origin $TAG || echo "ignoring this error. No tag to delete"
git tag $TAG
git push --tags
