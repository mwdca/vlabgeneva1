#!/bin/bash
# Copyright (c) 2019, Juniper Networks, Inc.
# All rights reserved.
#
# simple script to extract root passwords from vmx log file

if [ -z "$1" ]; then
  echo "$0 <junos cli command>"
  exit 1
fi

set -e # exit if something goes wrong

if [ "$USER" == "root" ]; then
  CLI=cli
fi

for device in vmx1 vmx2 spine1 spine2 leaf1 leaf2; do
  IP=$(docker-compose logs $device |grep ' root password' | grep '(v4:' |cut -d: -f2|cut -d' ' -f1)
  if [ -z "$IP" ]; then
    IP=$(docker-compose logs $device |grep ' root password '|cut -d\( -f2|cut -d\) -f1)
  fi
  echo ""
  echo "Executing \"$@\" on $device ($IP)... "
  ssh -i id_rsa -o StrictHostKeyChecking=no $IP "$@"
done
