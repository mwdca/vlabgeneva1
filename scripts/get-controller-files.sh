#!/bin/bash
# Copyright (c) 2018, Juniper Networks, Inc.
# All rights reserved.
#
# simple script to extract root passwords from vmx log file

set -e # exit if something goes wrong

source .env
DEST=tmplogs
mkdir -p $DEST

scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11:/var/log/contrail/contrail-fabric-ansible*.log $DEST/
