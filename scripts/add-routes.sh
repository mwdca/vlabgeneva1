#!/bin/bash
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.21 gw 172.16.2.41
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.22 gw 172.16.2.42

ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.31 gw 172.16.2.31
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.32 gw 172.16.2.32

ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.11 gw 172.16.2.51
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i id_rsa root@192.168.2.11 route add -host 10.6.0.12 gw 172.16.2.52
